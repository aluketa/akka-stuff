package stuff

import akka.actor.Actor
import com.google.common.util.concurrent.RateLimiter

trait BackPressureAwareProducer extends Actor {

  private val rateRecorder = new RateRecorder
  private val rateLimiter = RateLimiter.create(Double.MaxValue)

  protected def handleReceive: Receive

  override def receive: Receive = {
    case AverageProcessTime(millis) =>
      val permitsPerSecond = 1.0 / (millis / 1000.0)
      println(s"Average process time: ${millis}ms, setting rate to: ${permitsPerSecond}p/s")
      rateLimiter.setRate(permitsPerSecond)
      rateRecorder.averageProcessTime(millis)

    case msg @ _ =>
      if (rateLimiter.tryAcquire && rateRecorder.hasCaughtUp) {
        handleReceive(msg)
        rateRecorder.record()
      }
  }
}

class RateRecorder {
  var count = 0
  var started = 0L
  var expectedCaughtUpTime = 0L

  def record(): Unit = {
    if (count == 0) {
      started = System.currentTimeMillis()
    }

    count = count + 1
  }

  def averageProcessTime(t: Long): Unit = {
    if (hasCaughtUp) {
      expectedCaughtUpTime = started + (count * t)
      count = 0
      started = 0L
    }
  }

  def hasCaughtUp: Boolean = System.currentTimeMillis() > expectedCaughtUpTime
}

case class AverageProcessTime(millis: Long)
