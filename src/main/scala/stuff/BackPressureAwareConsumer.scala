package stuff

import akka.actor.Actor

trait BackPressureAwareConsumer extends Actor {

  protected val notificationCount: Int = 5
  private val averageTimeCalculator = new AverageTimeCalculator

  protected def handleReceive: Receive

  override def receive: Receive = {
    case msg @ _ =>
      averageTimeCalculator.start()
      val result = handleReceive(msg)
      averageTimeCalculator.stop()

      if (averageTimeCalculator.count > notificationCount) {
        sender() ! AverageProcessTime(averageTimeCalculator.averageTime)
        averageTimeCalculator.reset()
      }

      result
  }
}

class AverageTimeCalculator(var count: Int, var totalTime: Long) {

  def this() = this(0, 0)

  var started = 0L

  def start(): Unit = {
    started = System.currentTimeMillis()
  }

  def stop(): Unit = {
    count = count + 1
    totalTime = totalTime + (System.currentTimeMillis() - started)
  }

  def averageTime: Long = totalTime / count

  def reset(): Unit = {
    count = 0
    totalTime = 0
  }
}