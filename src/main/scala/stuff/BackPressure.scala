package stuff

import akka.actor.{Actor, ActorRef, ActorSystem, Props}
import com.google.common.util.concurrent.RateLimiter

import scala.concurrent.ExecutionContext
import scala.concurrent.duration.Duration.Zero
import scala.concurrent.duration.{Duration, SECONDS}

object BackPressure extends App {
  val system = ActorSystem.create("main")

  val consumer = system.actorOf(Props(classOf[Consumer]))
  val producer = system.actorOf(Props(classOf[Producer], consumer))

  system.scheduler.schedule(Zero, Duration(1, SECONDS), producer, Tick)(executor = ExecutionContext.global)
}

class Producer(consumer: ActorRef) extends BackPressureAwareProducer {

  var count = 0

  override def handleReceive: Receive = {
    case Tick =>
      val message = "Data: " + count
      println(s"Producer sending: $message")
      consumer ! Data(message)
      count = count + 1

    case u @ _ => println(s"Unknown message: $u")
  }
}

class Consumer extends BackPressureAwareConsumer {

  override def handleReceive: Receive = {
    case Data(message) =>
      Thread.sleep(3000)
      println(s"Consumer received: $message")
  }
}

case object Tick
case class Data(message: String)